package com.wookler.server.common.model;


import java.io.UnsupportedEncodingException;
import java.util.UUID;

/**
 * Entity type with byte[] as the key type.
 * <p/>
 * Created by subghosh on 3/15/15.
 */
public abstract class ByteKeyedEntity<T> extends Entity<byte[], T> {

    /**
     * Utility method to create a new instance of an entity and assign it a UUID based
     * key.
     *
     * @param type - Class type to create instance of.
     * @param <M>  - Object of class type specified.
     * @return - New instance.
     * @throws EntityException
     */
    @SuppressWarnings("unchecked")
	public static <M extends ByteKeyedEntity<?>> M create(Class<?> type) throws EntityException {
        try {
            Object o = type.newInstance();
            if (!(o instanceof ByteKeyedEntity))
                throw new EntityException("Invalid class type. [type=" + type
                        .getCanonicalName() + "]");
            M m = (M) o;
            String id = UUID.randomUUID().toString();
            m.setId(id.getBytes("UTF-8"));

            return m;
        } catch (InstantiationException e) {
            throw new EntityException("Error creating instance.", e);
        } catch (IllegalAccessException e) {
            throw new EntityException("Error creating instance.", e);
        } catch (UnsupportedEncodingException e) {
            throw new EntityException("Error creating instance.", e);
        }
    }
}
