/*
 *
 *  * Copyright 2014 Subhabrata Ghosh
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *     http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.wookler.server.common.model;

/**
 * Exception used to relay errors due to concurrent modification and the resulting
 * merge conflicts.
 * <p/>
 * <p/>
 * Created by subghosh on 16/02/14.
 */
@SuppressWarnings("serial")
public class MergeConflictException extends EntityException {
    private static final String _PREFIX_ = "Cannot resolve merge conflict : ";

    public MergeConflictException(String mesg) {
        super(_PREFIX_ + mesg);
    }

    public MergeConflictException(String mesg, Throwable inner) {
        super(_PREFIX_ + mesg, inner);
    }
}
