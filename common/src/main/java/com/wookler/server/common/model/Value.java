/*
 *
 *  Copyright 2014 Subhabrata Ghosh
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package com.wookler.server.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * TODO: <Write type description>
 *
 * @author subghosh
 * @created Apr 30, 2015:1:20:13 PM
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class Value<T> {
	@JsonIgnore
	protected T original;
	@JsonIgnore
	protected T current;
	@JsonIgnore
	protected long updateTime = -1;

	/**
	 * TODO : <Write comments>
	 *
	 * @return the original
	 */
	@JsonIgnore
	public T getOriginal() {
		return original;
	}

	/**
	 * TODO: <Write comments>
	 *
	 * @param original
	 *            the original to set
	 */
	@JsonIgnore
	public void setOriginal(T original) {
		this.original = original;
	}

	/**
	 * TODO : <Write comments>
	 *
	 * @return the value
	 */
	@JsonIgnore
	public T getCurrent() {
		return current;
	}

	/**
	 * TODO: <Write comments>
	 *
	 * @param value
	 *            the value to set
	 */
	@JsonIgnore
	public void setCurrent(T value) {
		this.current = value;
	}

	/**
	 * TODO : <Write comments>
	 *
	 * @return the updateTime
	 */
	@JsonIgnore
	public long getUpdateTime() {
		return updateTime;
	}

	/**
	 * TODO: <Write comments>
	 *
	 * @param updateTime
	 *            the updateTime to set
	 */
	@JsonIgnore
	public void setUpdateTime(long updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * TODO: <Write comments>
	 *
	 * @param value
	 */
	public void setValue(T value) {
		if (original == null && updateTime <= 0) {
			original = current;
			updateTime = System.currentTimeMillis();
		}
		this.current = value;
	}

	/**
	 * TODO: <Write comments>
	 *
	 * @return
	 */
	public T getValue() {
		return current;
	}

	/**
	 * Compare the two entity values and return the difference array.
	 * 
	 * <pre>
	 * Case 1: This has been modified, but target hasn't : return this' current
	 * value. 
	 * Case 2: Target has been modified but this hasn't : return target's
	 * current value. 
	 * Case 3: Both this and target have been modified : Throw version exception with both the values 
	 * 	if overwrite is not specified. If overwrite specified, return the latest update.
	 * Case 4: Original value different for this and target : Throw version exception.
	 * </pre>
	 * 
	 * @param target
	 *            - Target value to compare wtih.
	 * @param overwrite
	 *            - Overwrite the value based on the last updated.
	 * @return
	 */
	public T diff(Value<T> target, boolean overwrite)
			throws EntityVersionException {
		if (!compare(target.current, current)) {
			// Different source instances.
			if (!compare(original, target.original)) {
				throw new EntityVersionException(
						"Source version of value missmatch.");
			}
			// Target modified, source if the same.
			if (compare(target.original, current)) {
				return target.current;
			}
			// Source modified, target is the same.
			if (compare(original, target.current)) {
				return current;
			}
			// Both source and target value modified.
			if (overwrite) {
				if (updateTime > target.updateTime) {
					return current;
				} else {
					return target.current;
				}
			}
			throw new EntityVersionException(new Object[] { current,
					target.current });
		}
		return null;
	}
	

	@SuppressWarnings("unchecked")
	protected <K> boolean compare(K source, K target) {
		if (source == null && target == null) {
			return true;
		} else if (source == null && target != null) {
			return false;
		} else if (source != null && target == null) {
			return false;
		}
		Class<?> c = source.getClass();
		if (c.isPrimitive()) {
			return (source == target);
		} else if (c.equals(String.class)) {
			return source.equals(target);
		} else if (source instanceof Comparable) {
			return (((Comparable<K>) source).compareTo(target) == 0);
		}
		return false;
	}
}
