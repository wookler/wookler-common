package com.wookler.server.common.model;

/**
 * Wrapper class for encapsulating entity entities. This class manages the state
 * of the entity record to handle persistence and merge conflict resolution that
 * might arise due to the eventual consistency assumption.
 *
 * @param <T>
 *            - Type of entity encapsulated.
 */
public class ManagedEntity<T extends ByteKeyedEntity<?>> {
	public static enum EEntityState {
		/**
		 * Entity state is unknown.
		 */
		Unknown,
		/**
		 * Entity is in sync with the store.
		 */
		Synced,
		/**
		 * Entity state is new (has not been persisted)
		 */
		New,
		/**
		 * Entity has been updated.
		 */
		Updated,
		/**
		 * Entity has been deleted.
		 */
		Deleted
	}

	protected EEntityState state;
	protected long updatetime;
	protected long createtime;
	protected long readtime;
	protected T entity;
	protected boolean replicated = false;
	
	/**
	 * Is this instance a replicated instance.
	 * 
	 * @return the replicated
	 */
	public boolean isReplicated() {
		return replicated;
	}

	/**
	 * Set this instance is replicated.
	 * 
	 * @param replicated
	 *            the replicated to set
	 */
	public void setReplicated(boolean replicated) {
		this.replicated = replicated;
	}

	/**
	 * Get the entity instance state.
	 *
	 * @return - Entity state.
	 */
	public EEntityState getState() {
		return state;
	}

	/**
	 * Set the entity instance state.
	 *
	 * @param state
	 *            - Entity state.
	 */
	public void setState(EEntityState state) {
		this.state = state;
	}

	/**
	 * Get the entity creation time.
	 *
	 * @return - Entity creation time.
	 */
	public long getCreatetime() {
		return createtime;
	}

	/**
	 * Set the entity creation time.
	 *
	 * @param createtime
	 *            - Entity creation time.
	 */
	public void setCreatetime(long createtime) {
		this.createtime = createtime;
	}

	/**
	 * Get the entity last update time.
	 *
	 * @return - Last update time.
	 */
	public long getUpdatetime() {
		return updatetime;
	}

	/**
	 * Set the entity last update time.
	 *
	 * @param updatetime
	 *            - Last update time.
	 */
	public void setUpdatetime(long updatetime) {
		this.updatetime = updatetime;
	}

	/**
	 * Get the entity element associated with this entity.
	 *
	 * @return - Entity entity.
	 */
	public T getEntity() {
		return entity;
	}

	/**
	 * Set the entity element associated with this entity.
	 *
	 * @param entity
	 *            - Entity entity.
	 */
	public void setEntity(T entity) {
		this.entity = entity;
	}

	/**
	 * Get the timestamp when this record was read from the persistent store.
	 *
	 * @return - Record read timestamp.
	 */
	public long getReadtime() {
		return readtime;
	}

	/**
	 * Set the timestamp when this record was read from the persistent store.
	 *
	 * @param readtime
	 *            - Record read timestamp.
	 */
	public void setReadtime(long readtime) {
		this.readtime = readtime;
	}

	/**
	 * Create a copy of this instance of the entity.
	 *
	 * @return - Instance copy.
	 * @throws EntityException
	 */
	@SuppressWarnings("unchecked")
	public ManagedEntity<T> copy() throws EntityException {
		ManagedEntity<T> clone = new ManagedEntity<>();
		clone.state = this.state;
		clone.updatetime = this.updatetime;
		clone.createtime = this.createtime;
		clone.entity = (T) this.entity.copy();

		return clone;
	}

	/**
	 * Copy the header properties associated with this entity to the target.
	 *
	 * @param target
	 *            - Target entity to copy to.
	 */
	public void copyHeader(ManagedEntity<?> target) {
		target.state = state;
		target.createtime = createtime;
		target.updatetime = updatetime;
	}
}