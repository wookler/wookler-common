/*
 *
 *  Copyright 2014 Subhabrata Ghosh
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package com.wookler.server.common.model;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: <Write type description>
 *
 * @author subghosh
 * @created May 26, 2015:9:59:33 PM
 *
 */
public class ListValue<T> extends Value<List<T>> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.wookler.server.common.model.Value#diff(com.wookler.server.common.
	 * model.Value, boolean)
	 */
	@Override
	public List<T> diff(Value<List<T>> target, boolean overwrite)
			throws EntityVersionException {
		if (compare(current, target.current) != null) {
			// Different source instances.
			if (compare(original, target.original) != null)
				throw new EntityVersionException(
						"Source version of value missmatch.");
			// Target modified, source if the same.
			if (compare(target.original, current) == null) {
				return target.current;
			}
			// Source modified, target is the same.
			if (compare(original, target.current) == null) {
				return current;
			}
			// Both source and target value modified.
			if (overwrite) {
				if (updateTime > target.updateTime) {
					return current;
				} else {
					return target.current;
				}
			}
			throw new EntityVersionException(new Object[] { current,
					target.current });
		}
		return null;
	}

	private List<T> compare(List<T> source, List<T> target) {
		if ((source == null || source.isEmpty())
				&& (target != null && !target.isEmpty())) {
			return target;
		}
		if ((source != null && !source.isEmpty())
				&& (target == null || target.isEmpty())) {
			return source;
		}
		List<T> temp = new ArrayList<>(target);
		for (T s : source) {
			boolean found = false;
			for (T t : target) {
				if (compare(s, t)) {
					found = true;
					remove(t, temp);
					break;
				}
			}
			if (!found)
				temp.add(s);
		}
		return (temp.isEmpty() ? null : temp);
	}

	private void remove(T e, List<T> list) {
		int index = -1;
		for (int ii = 0; ii < list.size(); ii++) {
			if (compare(e, list.get(ii))) {
				index = ii;
				break;
			}
		}
		if (index >= 0)
			list.remove(index);
	}
}
