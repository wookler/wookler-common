/*
 *
 *  Copyright 2014 Subhabrata Ghosh
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package com.wookler.server.common.model;

/**
 * TODO: <Write type description>
 *
 * @author subghosh
 * @created May 26, 2015:2:17:59 PM
 *
 */
@SuppressWarnings("serial")
public class EntityVersionException extends EntityException {
	private static final String PREFIX = "Entity Version exception : ";
	private Object[] values;

	public EntityVersionException(String mesg) {
		super(String.format("%s%s", PREFIX, mesg));
	}

	public EntityVersionException(String mesg, Throwable t) {
		super(String.format("%s%s", PREFIX, mesg), t);
	}

	public EntityVersionException(Object[] values) {
		super(String.format("%s Source version mismatch.", PREFIX));
		this.values = values;
	}

	public Object[] values() {
		return values;
	}
}
