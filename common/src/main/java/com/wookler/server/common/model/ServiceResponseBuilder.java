/*
 *
 *  Copyright 2014 Subhabrata Ghosh
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package com.wookler.server.common.model;

import java.util.ArrayList;
import java.util.List;

import com.google.protobuf.ByteString;
import com.wookler.server.common.model.SecurityDataModelProto.ESecurityZone;
import com.wookler.server.common.model.ServiceDef.DataBuffer;
import com.wookler.server.common.model.ServiceDef.EEncryptionType;
import com.wookler.server.common.model.ServiceDef.EResponseCodes;
import com.wookler.server.common.model.ServiceDef.Property;
import com.wookler.server.common.model.ServiceDef.ResponseHeader;
import com.wookler.server.common.model.ServiceDef.ResponseState;
import com.wookler.server.common.model.ServiceDef.ServiceResponse;

/**
 * Builder class for creating service response protobuf instance.
 *
 * @author Subho Ghosh (subho dot ghosh at outlook.com)
 * 
 *         9:36:02 PM
 *
 */
public class ServiceResponseBuilder {
	private ServiceResponse.Builder builder = ServiceResponse.newBuilder();
	private ResponseHeader.Builder header = ResponseHeader.newBuilder();
	private ResponseState state;
	private List<DataBuffer> buffers = null;

	public ServiceResponseBuilder(String requestId, String path, String command) {
		header.setRequestId(requestId).setPath(path).setCommand(command)
				.setTimestamp(System.currentTimeMillis());
	}

	public ServiceResponseBuilder withDataBuffer(DataBuffer buffer) {
		if (buffers == null)
			buffers = new ArrayList<>();
		buffers.add(buffer);
		return this;
	}

	public ServiceResponseBuilder withParam(Property param) {
		header.addParams(param);

		return this;
	}

	public ServiceResponseBuilder withParams(List<Property> params) {
		header.addAllParams(params);

		return this;
	}

	public ServiceResponseBuilder withParam(String key, String value) {
		Property.Builder b = Property.newBuilder();
		b.setKey(key).setValue(value);
		return withParam(b.build());
	}

	public ServiceResponseBuilder withResponseState(EResponseCodes state) {
		ResponseState.Builder b = ResponseState.newBuilder();
		b.setResponseCode(state);
		this.state = b.build();
		return this;
	}

	public ServiceResponseBuilder withResponseState(String componentId,
			EResponseCodes state, int errorCode, Throwable error) {
		this.state = ErrorResponseBuilder.errorResponse(componentId, state,
				errorCode, error);
		return this;
	}

	public ServiceResponseBuilder withDataBuffer(ESecurityZone currentZone,
			ESecurityZone targetZone, Class<?> type, int size, byte[] data,
			EEncryptionType encryptionType) {
		DataBuffer.Builder b = DataBuffer.newBuilder();
		b.setEncryptionType(encryptionType).setSenderZone(currentZone)
				.setTargetZone(targetZone).setSize(size)
				.setType(type.getCanonicalName())
				.setBody(ByteString.copyFrom(data));
		return withDataBuffer(b.build());
	}

	public ServiceResponseBuilder withMessage(String message) {
		builder.setMessage(message);
		return this;
	}

	public ServiceResponse build() throws ServiceException {
		builder.setHeader(header).setState(state);
		if (buffers != null) {
			for (DataBuffer b : buffers) {
				builder.addBuffers(b);
			}
		}
		return builder.build();
	}
}
