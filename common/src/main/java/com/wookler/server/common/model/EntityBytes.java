package com.wookler.server.common.model;

import com.google.protobuf.ByteString;

import java.io.IOException;

/**
 * Default byte entity representation of the entities, used for persistence.
 * <p/>
 * Created by Subho on 2/20/2015.
 */
public class EntityBytes extends ByteKeyedEntity<byte[]> {

    /**
     * Get the byte entity.
     *
     * @return - byte[]
     */
    public byte[] getData() {
        return data;
    }

    /**
     * Set the byte entity.
     *
     * @param data - byte[]
     */
    public EntityBytes setData(byte[] data) {
        this.data = data;

        return this;
    }

    /**
     * Merge the entity from the specified target to the current instance.
     *
     * @param target - Target to merge entity from.
     * @throws com.wookler.server.common.model.EntityException
     */
    @Override
    public void merge(Object target) throws EntityException {
        throw new EntityException("Operation not implemented.");
    }

    /**
     * Create a copy of the current instance.
     *
     * @return - Copy instance.
     * @throws com.wookler.server.common.model.EntityException
     */
    @Override
    public Object copy() throws EntityException {
        if (data != null && data.length > 0) {
            EntityBytes ne = new EntityBytes();
            ne.id = new byte[id.length];
            System.arraycopy(id, 0, ne.id, 0, id.length);
            ne.data = new byte[data.length];
            System.arraycopy(data, 0, ne.data, 0, ne.data.length);

            return ne;
        }
        return null;
    }

    /**
     * Compute and return a delta object comparing the current instance with the source.
     *
     * @param source - Source instance to compare to.
     * @return - Delta object.
     * @throws com.wookler.server.common.model.EntityException
     */
    @Override
    public byte[] delta(byte[] source) throws EntityException {
        throw new EntityException("Operation not implemented.");
    }

    /**
     * Get the serializer associated with this entity.
     *
     * @return - Serializer.
     */
    @Override
    public Serializer<byte[]> serializer() {
        return ByteSerializer.instance;
    }

    public static final class ByteSerializer implements Serializer<byte[]> {
        public static final ByteSerializer instance = new ByteSerializer();

        /**
         * Serialize the entity object to a byte array.
         *
         * @param data - Data element to serialize.
         * @return - Serialized byte[] array.
         * @throws java.io.IOException
         */
        @Override
        public byte[] serialize(byte[] data) throws IOException {
            return data;
        }

        /**
         * De-serialize the byte array to the entity element.
         *
         * @param data - Byte array.
         * @return - Data element.
         * @throws java.io.IOException
         */
        @Override
        public byte[] deserialize(byte[] data) throws IOException {
            return data;
        }

        /**
         * Can serialize the specified type.
         *
         * @param type - Type to check.
         * @return - Can serialize?
         */
        @Override
        public boolean accept(Class<?> type) {
            return false;
        }
    }

    public static final class EntityByteSerializer implements Serializer<EntityBytes> {

        /**
         * Serialize the entity object to a byte array.
         *
         * @param data - Data element to serialize.
         * @return - Serialized byte[] array.
         * @throws java.io.IOException
         */
        @Override
        public byte[] serialize(EntityBytes data) throws IOException {
            EntityByteBuf.EntityByteProto ep = EntityByteBuf.EntityByteProto.newBuilder()
                    .setId(ByteString.copyFrom(data.getId())).setData(ByteString
                            .copyFrom(data.getData())).build();
            return ep.toByteArray();
        }

        /**
         * De-serialize the byte array to the entity element.
         *
         * @param data - Byte array.
         * @return - Data element.
         * @throws java.io.IOException
         */
        @Override
        public EntityBytes deserialize(byte[] data) throws IOException {
            EntityByteBuf.EntityByteProto ep = EntityByteBuf.EntityByteProto.parseFrom
                    (data);
            EntityBytes eb = new EntityBytes();
            eb.setId(ep.getId().toByteArray());
            eb.setData(ep.getData().toByteArray());

            return eb;
        }

        /**
         * Can serialize the specified type.
         *
         * @param type - Type to check.
         * @return - Can serialize?
         */
        @Override
        public boolean accept(Class<?> type) {
            return type.equals(EntityBytes.class);
        }
    }
}
