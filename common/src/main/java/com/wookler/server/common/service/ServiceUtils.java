/*
 *
 *  Copyright 2014 Subhabrata Ghosh
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package com.wookler.server.common.service;

import java.util.List;

import com.wookler.server.common.model.ExceptionDef.ErrorProto;
import com.wookler.server.common.model.ExceptionDef.ExceptionProto;
import com.wookler.server.common.model.ExceptionDef.StackTraceElementProto;
import com.wookler.server.common.model.SecurityDataModelProto.ESecurityZone;
import com.wookler.server.common.model.ServiceDef.DataBuffer;
import com.wookler.server.common.model.ServiceDef.EResponseCodes;
import com.wookler.server.common.model.ServiceDef.Property;
import com.wookler.server.common.model.ServiceDef.RequestHeader;
import com.wookler.server.common.model.ServiceDef.ResponseState;
import com.wookler.server.common.model.ServiceDef.ServiceRequest;
import com.wookler.server.common.model.ServiceDef.ServiceResponse;

/**
 * Utility class, provides methods to process service response/requests.
 *
 * @author Subho Ghosh (subho dot ghosh at outlook.com)
 * 
 *         2:13:58 PM
 *
 */
public class ServiceUtils {
	/**
	 * Exception class to represent exceptions raised by back-end services.
	 * Basically converts the serialized protobuf exception/stacktrace for
	 * consumption.
	 *
	 * @author Subho Ghosh (subho dot ghosh at outlook.com)
	 * 
	 *         11:30:14 AM
	 *
	 */
	@SuppressWarnings("serial")
	public static class ServiceResponseException extends Exception {
		private String requestId;
		private String path;
		private String command;
		private EResponseCodes code;

		public ServiceResponseException(String mesg) {
			super(mesg);
		}

		public ServiceResponseException(String mesg, Throwable inner) {
			super(mesg, inner);
		}

		/**
		 * @return the requestId
		 */
		public String getRequestId() {
			return requestId;
		}

		/**
		 * @param requestId
		 *            the requestId to set
		 */
		public void setRequestId(String requestId) {
			this.requestId = requestId;
		}

		/**
		 * @return the path
		 */
		public String getPath() {
			return path;
		}

		/**
		 * @param path
		 *            the path to set
		 */
		public void setPath(String path) {
			this.path = path;
		}

		/**
		 * @return the command
		 */
		public String getCommand() {
			return command;
		}

		/**
		 * @param command
		 *            the command to set
		 */
		public void setCommand(String command) {
			this.command = command;
		}

		/**
		 * @return the code
		 */
		public EResponseCodes getCode() {
			return code;
		}

		/**
		 * @param code
		 *            the code to set
		 */
		public void setCode(EResponseCodes code) {
			this.code = code;
		}
	}

	/**
	 * Create a new instance of a Response Error based on the protobuf
	 * serialized service exception.
	 * 
	 * @param response
	 *            - Service response.
	 * @return - Response error instance.
	 */
	public static final ServiceResponseException newResponseError(
			ServiceResponse response) {
		ResponseState rs = response.getState();
		if (rs.hasError()) {
			ErrorProto error = rs.getError();
			StringBuffer b = new StringBuffer();
			if (error.hasComponentId()) {
				b.append(String.format("[COMPONENT : %s]",
						error.getComponentId()));
			}
			if (error.hasException()) {
				b.append(error.getException().getErrorCode()).append(":")
						.append(error.getException().getMesg());
			}
			ServiceResponseException e = null;
			if (error.hasException()) {
				Exception cause = protoToException(error.getException());
				e = new ServiceResponseException(b.toString(), cause);
			} else {
				e = new ServiceResponseException(b.toString());
			}
			e.requestId = response.getHeader().getRequestId();
			e.code = rs.getResponseCode();
			e.path = response.getHeader().getPath();
			e.command = response.getHeader().getCommand();
			return e;
		}
		return null;
	}

	/**
	 * Convert a protobuf serialized exception/stacktrace to an exception
	 * instance.
	 * 
	 * @param e
	 *            - Protobuf serialized exception.
	 * @return - Java exception type.
	 */
	public static final Exception protoToException(ExceptionProto e) {
		String mesg = String.format("[%s] %s", e.getErrorCode(), e.getMesg());
		Exception inner = null;
		if (e.hasCause()) {
			inner = protoToException(e.getCause());
		}
		Exception ex = null;
		if (inner == null) {
			ex = new Exception(mesg);
		} else {
			ex = new Exception(mesg, inner);
		}
		if (e.getStacktraceCount() > 0) {
			StackTraceElement[] elements = new StackTraceElement[e
					.getStacktraceCount()];
			for (int ii = 0; ii < e.getStacktraceCount(); ii++) {
				StackTraceElementProto sp = e.getStacktrace(ii);
				StackTraceElement el = new StackTraceElement(sp.getClassname(),
						sp.getMethod(), sp.getFile(), sp.getLine());
				elements[ii] = el;
			}
			ex.setStackTrace(elements);
		}
		return ex;
	}

	/**
	 * Get the data buffer from the service response for the specified zone.
	 * 
	 * @param zone
	 *            - Security Zone
	 * @param response
	 *            - Service response.
	 * @return - Data Buffer or NULL if none found for the zone.
	 */
	public static final DataBuffer getDataBuffer(ESecurityZone zone,
			ServiceResponse response) {
		if (response.getBuffersCount() > 0) {
			List<DataBuffer> buffers = response.getBuffersList();
			for (DataBuffer b : buffers) {
				if (b.hasTargetZone()) {
					if (b.getTargetZone() == zone) {
						return b;
					}
				}
			}
		}
		return null;
	}

	public static final Property findProperty(ServiceRequest request,
			String name) {
		RequestHeader header = request.getHeader();
		if (header.getPropertiesCount() > 0) {
			List<Property> properties = header.getPropertiesList();
			for (Property p : properties) {
				if (p.hasKey()) {
					if (p.getKey().compareTo(name) == 0) {
						return p;
					}
				}
			}
		}
		return null;
	}

	public static final Property findParam(ServiceRequest request, String name) {
		if (request.getParamsCount() > 0) {
			List<Property> properties = request.getParamsList();
			for (Property p : properties) {
				if (p.hasKey()) {
					if (p.getKey().compareTo(name) == 0) {
						return p;
					}
				}
			}
		}
		return null;
	}
}
