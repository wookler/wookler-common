package com.wookler.server.common.model;

import com.wookler.server.common.structs.ACL;

/**
 * Abstract base class to be extended by Managed Entities to handle entity merge
 * due to eventual consistency assumptions.
 * <p/>
 * Created by Subho on 2/19/2015.
 */
public abstract class Entity<K, M> implements Cloneable {
	protected K id;
	protected M data;
	protected ACL acl = new ACL();
	protected Serializer<M> serializer;
	protected long version;

	/**
	 * Get the unique ID associated with this entity instance. ID(s) are assumed
	 * to be unique per class type.
	 *
	 * @return
	 */
	public K getId() {
		return id;
	}

	/**
	 * Set the unique ID for this entity instance.
	 *
	 * @param id
	 *            - Unqiue ID.
	 * @return - Self
	 */
	public Entity<K, M> setId(K id) {
		this.id = id;

		return this;
	}

	/**
	 * Set the entity object for this entity.
	 *
	 * @param data
	 *            - Data object.
	 * @return - Self.
	 */
	public Entity<K, M> setData(M data) {
		this.data = data;

		return this;
	}

	/**
	 * Get the edit version number for this entity.
	 *
	 * @return - Edit version#.
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * Set the edit version number for this entity.
	 *
	 * @param version
	 *            - Edit version#.
	 * @return - Self.
	 */
	public Entity<K, M> setVersion(long version) {
		this.version = version;

		return this;
	}

	public Entity<K, M> incrementVersion() {
		version++;

		return this;
	}

	/**
	 * Get the entity object for this entity.
	 *
	 * @return - Data object.
	 */
	public M getData() {
		return data;
	}

	/**
	 * Get the ACL associated with this instance.
	 *
	 * @return - Instance ACL.
	 */
	public ACL getAcl() {
		return acl;
	}

	/**
	 * Set the ACL associated with this instance.
	 *
	 * @param acl
	 *            - Instance ACL.
	 */
	public void setAcl(ACL acl) {
		this.acl = acl;
	}

	/**
	 * Get the serializer associated with this entity.
	 *
	 * @return - Serializer.
	 */
	public abstract Serializer<M> serializer();

	/**
	 * Merge the entity from the specified target to the current instance.
	 *
	 * @param target
	 *            - Target to merge entity from.
	 * @throws com.wookler.server.common.model.EntityException
	 */
	public abstract void merge(Object target) throws EntityException;

	/**
	 * Compute and return a delta object comparing the current instance with the
	 * source.
	 *
	 * @param source
	 *            - Source instance to compare to.
	 * @return - Delta object.
	 * @throws EntityException
	 */
	public abstract M delta(M source) throws EntityException;

	/**
	 * Create a copy of the current instance.
	 *
	 * @return - Copy instance.
	 * @throws EntityException
	 */
	public abstract Object copy() throws EntityException;
}
