/*
 *
 *  Copyright 2014 Subhabrata Ghosh
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *  http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package com.wookler.server.common.model;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wookler.server.common.model.ExceptionDef.ErrorProto;
import com.wookler.server.common.model.ExceptionDef.ExceptionProto;
import com.wookler.server.common.model.ExceptionDef.StackTraceElementProto;
import com.wookler.server.common.model.ServiceDef.EResponseCodes;
import com.wookler.server.common.model.ServiceDef.ResponseState;
import com.wookler.server.common.service.FailedServiceCall;

/**
 * Utility class to convert Exception/StackTrace to protobuf definition.
 *
 * @author Subho Ghosh (subho dot ghosh at outlook.com)
 * 
 *         12:55:55 PM
 *
 */
public class ErrorResponseBuilder {
	private static final Logger LOG = LoggerFactory
			.getLogger(ErrorResponseBuilder.class);

	/**
	 * Create a new Service response protobuf with the specified excpetion.
	 * 
	 * @param request
	 *            - Service request context.
	 * @param t
	 *            - Exception.
	 * @return - Service Response context.
	 */
	public static ResponseState errorResponse(String componentId,
			int errorCode, Throwable t) {
		ResponseState.Builder sb = ResponseState.newBuilder();
		if (t instanceof FailedServiceCall) {
			sb.setResponseCode(((FailedServiceCall) t).getResponseCode());
		} else
			sb.setResponseCode(EResponseCodes.FAILED);
		ErrorProto e = error(t, componentId, errorCode);
		if (e != null) {
			sb.setError(e);
		}
		return sb.build();
	}

	public static ResponseState errorResponse(String componentId,
			EResponseCodes state, int errorCode, Throwable t) {
		ResponseState.Builder sb = ResponseState.newBuilder();
		sb.setResponseCode(state);
		ErrorProto e = error(t, componentId, errorCode);
		if (e != null) {
			sb.setError(e);
		}
		return sb.build();
	}

	public static ErrorProto error(Throwable e, String componentId,
			int errorCode) {
		ErrorProto.Builder b = ErrorProto.newBuilder();
		b.setException(exception(e, errorCode)).setComponentId(componentId);
		return b.build();
	}

	private static ExceptionProto exception(Throwable e, int errorCode) {
		ExceptionProto.Builder exb = ExceptionProto.newBuilder();
		if (e == null || StringUtils.isEmpty(e.getLocalizedMessage())) {
			exb.setMesg("Unknown error.").setErrorCode(errorCode).build();
		} else
			exb.setMesg(e.getLocalizedMessage()).setErrorCode(errorCode)
					.build();
		if (LOG.isDebugEnabled()) {
			StackTraceElementProto[] trace = stacktrace(e);
			if (trace != null && trace.length > 0) {
				for (int ii = 0; ii < trace.length; ii++) {
					exb.addStacktrace(trace[ii]);
				}
			}
			if (e.getCause() != null) {
				exb.setCause(exception(e.getCause(), errorCode));
			}
		}
		return exb.build();
	}

	private static StackTraceElementProto[] stacktrace(Throwable e) {
		if (e == null)
			return null;
		StackTraceElement[] stes = e.getStackTrace();
		if (stes == null || stes.length <= 0)
			return null;
		StackTraceElementProto[] elements = new StackTraceElementProto[stes.length];
		for (int ii = 0; ii < stes.length; ii++) {
			StackTraceElement st = stes[ii];
			StackTraceElementProto stp = StackTraceElementProto.newBuilder()
					.setClassname(st.getClassName()).setFile(st.getFileName())
					.setMethod(st.getMethodName()).setLine(st.getLineNumber())
					.setIsNativeMethod(st.isNativeMethod()).build();
			elements[ii] = stp;
		}
		return elements;
	}

}
